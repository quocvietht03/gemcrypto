<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Saving explained
 */
add_filter('acf/settings/save_json', 'gemcrypto_acf_json_save_point');

function gemcrypto_acf_json_save_point( $path ) {
    
    // update path
    $path = get_stylesheet_directory() . '/acf-json';


    // return
    return $path;

}

/**
 * Loading Explained
 */
add_filter('acf/settings/load_json', 'gemcrypto_acf_json_load_point');

function gemcrypto_acf_json_load_point( $paths ) {

    // remove original path (optional)
    unset($paths[0]);


    // append path
    $paths[] = get_stylesheet_directory() . '/acf-json';


    // return
    return $paths;

}
