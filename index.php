<?php
/**
 * The site's entry point.
 *
 * Loads the relevant template part,
 * the loop is executed (when needed) by the relevant template part.
 *
 * @package GemCryptoElementor
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();

$is_elementor_theme_exist = function_exists( 'elementor_theme_do_location' );

if ( is_singular( 'post' ) ) {
	if ( ! $is_elementor_theme_exist || ! elementor_theme_do_location( 'single' ) ) {
		get_template_part( 'template-parts/single' );
	}
} elseif( is_singular( 'projects' ) ) {
	if ( ! $is_elementor_theme_exist || ! elementor_theme_do_location( 'single-projects' ) ) {
		get_template_part( 'template-parts/single-projects' );
	}
} elseif( is_singular( 'members' ) ) {
	if ( ! $is_elementor_theme_exist || ! elementor_theme_do_location( 'single-members' ) ) {
		get_template_part( 'template-parts/single-members' );
	}
} elseif ( is_archive() || is_home() ) {
	if ( ! $is_elementor_theme_exist || ! elementor_theme_do_location( 'archive' ) ) {
		if ( 'projects' == get_post_type() ) {
			get_template_part( 'template-parts/archive-projects' );
		} elseif ( 'members' == get_post_type() ) {
			get_template_part( 'template-parts/archive-members' );
		} else {
			get_template_part( 'template-parts/archive' );
		}
	}
} elseif ( is_search() ) {
	if ( ! $is_elementor_theme_exist || ! elementor_theme_do_location( 'archive' ) ) {
		get_template_part( 'template-parts/search' );
	}
} else {
	if ( ! $is_elementor_theme_exist || ! elementor_theme_do_location( 'single' ) ) {
		get_template_part( 'template-parts/404' );
	}
}

get_footer();
