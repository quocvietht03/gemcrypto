=== GemCrypto ===

Compatible with WordPress 5.7.x.
License: GNU General Public License v3 or later
License URI: https://www.gnu.org/licenses/gpl-3.0.html
Tags: custom-menu, custom-logo, featured-images, rtl-language-support, threaded-comments, translation-ready

A lightweight, plain-vanilla theme for Elementor page builder.

***GemCrypto*** is distributed under the terms of the GNU GPL v3 or later.

== Description ==

A basic, plain-vanilla, lightweight theme, best suited for building your site using Elementor page builder.

This theme resets the WordPress environment and prepares it for smooth operation of Elementor.

== Installation ==

1. In your site's admin panel, go to Appearance > Themes and click `Add New`.
2. Type "GemCrypto" in the search field.
3. Click `Install` and then `Activate` to start using the theme.
4. Navigate to Appearance > Customize in your admin panel and customize to your needs.
5. A notice box may appear, recommending you to install Elementor Page Builder Plugin. You can either use it or any other editor.
6. Create a new page, click `Edit with Elementor`.
7. Once the Elementor Editor is launched, click on the library icon, pick one of the many ready-made templates and click `Insert`.
8. Edit the page content as you wish, you can add, remove and manipulate any of the elements.
9. Enjoy :)

== Changelog ==

= 1.0.0 - 2022-03-04 =
* Initial Public Release
