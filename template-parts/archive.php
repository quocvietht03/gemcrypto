<?php
/**
 * The template for displaying archive pages.
 *
 * @package GemCryptoElementor
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>
<main class="site-main" role="main">

	<?php if ( apply_filters( 'gemcrypto_elementor_page_title', true ) ) : ?>
		<header class="page-header">
			<?php
			the_archive_title( '<h1 class="entry-title">', '</h1>' );
			the_archive_description( '<p class="archive-description">', '</p>' );
			?>
		</header>
	<?php endif; ?>
	<div class="page-content">
		<?php
		while ( have_posts() ) {
			the_post();
			$post_link = get_permalink();
			?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<header class="entry-header">
					<?php
					if ( is_sticky() && is_home() && ! is_paged() ) {
						printf( '<span class="sticky-post">%s</span>', _x( 'Featured', 'post', 'gemcrypto' ) );
					}
					the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
					?>
				</header><!-- .entry-header -->

				<?php if( has_post_thumbnail() ) { ?>
					<figure class="entry-thumbnail">
						<a class="entry-thumbnail-inner" href="<?php the_permalink(); ?>" aria-hidden="true" tabindex="-1">
							<?php the_post_thumbnail( 'large' ); ?>
						</a>
					</figure><!-- .entry-thumbnail -->
				<?php } ?>

				<div class="entry-content">
					<?php the_excerpt(); ?>
				</div><!-- .entry-content -->

				<footer class="entry-footer">
					<?php
						// Posted by.
						printf(
							/* translators: 1: SVG icon. 2: Post author, only visible to screen readers. 3: Author link. */
							'<span class="byline">%1$s<span class="screen-reader-text">%2$s</span><span class="author vcard"><a class="url fn n" href="%3$s">%4$s</a></span></span>',
							'<svg viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
							    <path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z"></path>
							    <path d="M0 0h24v24H0z" fill="none"></path>
							</svg>',
							__( 'Posted by', 'gemcrypto' ),
							esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
							esc_html( get_the_author() )
						);

						// Posted on.
						$time_string = sprintf(
							'<time class="entry-date published updated" datetime="%1$s">%2$s</time>',
							esc_attr( get_the_date( DATE_W3C ) ),
							esc_html( get_the_date() )
						);

						printf(
							'<span class="posted-on">%1$s<a href="%2$s" rel="bookmark">%3$s</a></span>',
							'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
							    <defs>
							        <path id="a" d="M0 0h24v24H0V0z"></path>
							    </defs>
							    <clipPath id="b">
							        <use xlink:href="#a" overflow="visible"></use>
							    </clipPath>
							    <path clip-path="url(#b)" d="M12 2C6.5 2 2 6.5 2 12s4.5 10 10 10 10-4.5 10-10S17.5 2 12 2zm4.2 14.2L11 13V7h1.5v5.2l4.5 2.7-.8 1.3z"></path>
							</svg>',
							esc_url( get_permalink() ),
							$time_string
						);

						/* translators: Used between list items, there is a space after the comma. */
						$categories_list = get_the_category_list( __( ', ', 'gemcrypto' ) );
						if ( $categories_list ) {
							printf(
								/* translators: 1: SVG icon. 2: Posted in label, only visible to screen readers. 3: List of categories. */
								'<span class="cat-links">%1$s<span class="screen-reader-text">%2$s</span>%3$s</span>',
								'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
								    <path d="M10 4H4c-1.1 0-1.99.9-1.99 2L2 18c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V8c0-1.1-.9-2-2-2h-8l-2-2z"></path>
								    <path d="M0 0h24v24H0z" fill="none"></path>
								</svg>',
								__( 'Posted in', 'gemcrypto' ),
								$categories_list
							); // WPCS: XSS OK.
						}

						/* translators: Used between list items, there is a space after the comma. */
						$tags_list = get_the_tag_list( '', __( ', ', 'gemcrypto' ) );
						if ( $tags_list && ! is_wp_error( $tags_list ) ) {
							printf(
								/* translators: 1: SVG icon. 2: Posted in label, only visible to screen readers. 3: List of tags. */
								'<span class="tags-links">%1$s<span class="screen-reader-text">%2$s </span>%3$s</span>',
								'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
									<path d="M21.41 11.58l-9-9C12.05 2.22 11.55 2 11 2H4c-1.1 0-2 .9-2 2v7c0 .55.22 1.05.59 1.42l9 9c.36.36.86.58 1.41.58.55 0 1.05-.22 1.41-.59l7-7c.37-.36.59-.86.59-1.41 0-.55-.23-1.06-.59-1.42zM5.5 7C4.67 7 4 6.33 4 5.5S4.67 4 5.5 4 7 4.67 7 5.5 6.33 7 5.5 7z"></path>
									<path d="M0 0h24v24H0z" fill="none"></path>
								</svg>',
								__( 'Tags:', 'gemcrypto' ),
								$tags_list
							); // WPCS: XSS OK.
						}

						// Comment count.
						if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
							echo '<span class="comments-link">';
							echo '<svg viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
							    <path d="M21.99 4c0-1.1-.89-2-1.99-2H4c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h14l4 4-.01-18z"></path>
							    <path d="M0 0h24v24H0z" fill="none"></path>
							</svg>';

							/* translators: %s: Post title. Only visible to screen readers. */
							comments_popup_link( sprintf( __( 'Leave a comment<span class="screen-reader-text"> on %s</span>', 'gemcrypto' ), get_the_title() ) );

							echo '</span>';
						}
					?>
				</footer><!-- .entry-footer -->
			</article><!-- #post-<?php the_ID(); ?> -->
		<?php } ?>
	</div>

	<?php wp_link_pages(); ?>

	<?php
		// Posts pagination.
		the_posts_pagination(
			array(
				'mid_size'  => 2,
				'prev_text' => sprintf(
					'%s <span class="nav-prev-text">%s</span>',
					'<svg viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
					    <path d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z"></path>
					    <path d="M0 0h24v24H0z" fill="none"></path>
					</svg>',
					__( 'Prev', 'gemcrypto' )
				),
				'next_text' => sprintf(
					'<span class="nav-next-text">%s</span> %s',
					__( 'Next', 'gemcrypto' ),
					'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
					    <path d="M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z"></path>
					    <path d="M0 0h24v24H0z" fill="none"></path>
					</svg>'
				),
			)
		);
	?>
</main>
