<?php
/**
 * The template for displaying singular post-types: posts, pages and user-defined custom post types.
 *
 * @package GemCryptoElementor
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>
<?php
while ( have_posts() ) :
	the_post();
	?>

<main class="site-main" role="main">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php if ( apply_filters( 'gemcrypto_elementor_page_title', true ) ) : ?>
			<header class="entry-header">
				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

				<div class="entry-meta">
					<?php
						// Posted by.
						printf(
							/* translators: 1: SVG icon. 2: Post author, only visible to screen readers. 3: Author link. */
							'<span class="byline">%1$s<span class="screen-reader-text">%2$s</span><span class="author vcard"><a class="url fn n" href="%3$s">%4$s</a></span></span>',
							'<svg viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
									<path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z"></path>
									<path d="M0 0h24v24H0z" fill="none"></path>
							</svg>',
							__( 'Posted by', 'gemcrypto' ),
							esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
							esc_html( get_the_author() )
						);

						// Posted on.
						$time_string = sprintf(
							'<time class="entry-date published updated" datetime="%1$s">%2$s</time>',
							esc_attr( get_the_date( DATE_W3C ) ),
							esc_html( get_the_date() )
						);

						printf(
							'<span class="posted-on">%1$s<a href="%2$s" rel="bookmark">%3$s</a></span>',
							'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
							    <defs>
							        <path id="a" d="M0 0h24v24H0V0z"></path>
							    </defs>
							    <clipPath id="b">
							        <use xlink:href="#a" overflow="visible"></use>
							    </clipPath>
							    <path clip-path="url(#b)" d="M12 2C6.5 2 2 6.5 2 12s4.5 10 10 10 10-4.5 10-10S17.5 2 12 2zm4.2 14.2L11 13V7h1.5v5.2l4.5 2.7-.8 1.3z"></path>
							</svg>',
							esc_url( get_permalink() ),
							$time_string
						);

						/* translators: Used between list items, there is a space after the comma. */
						$categories_list = get_the_category_list( __( ', ', 'gemcrypto' ) );
						if ( $categories_list ) {
							printf(
								/* translators: 1: SVG icon. 2: Posted in label, only visible to screen readers. 3: List of categories. */
								'<span class="cat-links">%1$s<span class="screen-reader-text">%2$s</span>%3$s</span>',
								'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
										<path d="M10 4H4c-1.1 0-1.99.9-1.99 2L2 18c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V8c0-1.1-.9-2-2-2h-8l-2-2z"></path>
										<path d="M0 0h24v24H0z" fill="none"></path>
								</svg>',
								__( 'Posted in', 'gemcrypto' ),
								$categories_list
							); // WPCS: XSS OK.
						}

						// Comment count.
						if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
							echo '<span class="comments-link">';
							echo '<svg viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
									<path d="M21.99 4c0-1.1-.89-2-1.99-2H4c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h14l4 4-.01-18z"></path>
									<path d="M0 0h24v24H0z" fill="none"></path>
							</svg>';

							/* translators: %s: Post title. Only visible to screen readers. */
							comments_popup_link( sprintf( __( 'Leave a comment<span class="screen-reader-text"> on %s</span>', 'gemcrypto' ), get_the_title() ) );

							echo '</span>';
						}
					?>
				</div><!-- .entry-meta -->
			</header>
		<?php endif; ?>

		<?php if( has_post_thumbnail() ) { ?>
			<figure class="entry-thumbnail">
				<a class="entry-thumbnail-inner" href="<?php the_permalink(); ?>" aria-hidden="true" tabindex="-1">
					<?php the_post_thumbnail( 'large' ); ?>
				</a>
			</figure><!-- .entry-thumbnail -->
		<?php } ?>

		<div class="entry-content">
			<?php the_content(); ?>

			<?php
				wp_link_pages(
					array(
						'before' => '<div class="page-links">' . __( 'Pages:', 'gemcrypto' ),
						'after'  => '</div>',
					)
				);
			?>
		</div>

		<footer class="entry-footer">
			<?php
				/* translators: Used between list items, there is a space after the comma. */
				$tags_list = get_the_tag_list( '', __( ', ', 'gemcrypto' ) );
				if ( $tags_list && ! is_wp_error( $tags_list ) ) {
					printf(
						/* translators: 1: SVG icon. 2: Posted in label, only visible to screen readers. 3: List of tags. */
						'<div class="tags-links">%1$s %2$s</div>',
						__( 'Tags:', 'gemcrypto' ),
						$tags_list
					); // WPCS: XSS OK.
				}
			?>
		</footer><!-- .entry-footer -->

		<?php if ( ! is_singular( 'attachment' ) ) : ?>
			<?php if ( (bool) get_the_author_meta( 'description' ) ) : ?>
				<div class="author-bio">
					<div class="author-avatar">
						<?php echo get_avatar( get_the_author_meta( 'ID' ), 120 ); ?>
					</div>
					<div class="author-content">
						<h2 class="author-title">
							<span class="author-heading">
								<?php
								printf(
									/* translators: %s: Post author. */
									__( 'Published by %s', 'gemcrypto' ),
									esc_html( get_the_author() )
								);
								?>
							</span>
						</h2>
						<p class="author-description">
							<?php the_author_meta( 'description' ); ?>
						</p><!-- .author-description -->
						<a class="author-link" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" rel="author">
							<?php _e( 'View more posts', 'gemcrypto' ); ?>
						</a>
					</div>
				</div><!-- .author-bio -->
			<?php endif; ?>
		<?php endif; ?>
	</article><!-- #post-<?php the_ID(); ?> -->

	<?php
		if ( is_singular( 'attachment' ) ) {
			// Parent post navigation.
			the_post_navigation(
				array(
					/* translators: %s: Parent post link. */
					'prev_text' => sprintf( __( '<span class="meta-nav">Published in</span><span class="post-title">%s</span>', 'gemcrypto' ), '%title' ),
				)
			);
		} elseif ( is_singular( 'post' ) ) {
			// Previous/next post navigation.
			the_post_navigation(
				array(
					'next_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Next Post', 'gemcrypto' ) . '</span> ' .
						'<span class="screen-reader-text">' . __( 'Next post:', 'gemcrypto' ) . '</span> <br/>' .
						'<span class="post-title">%title</span>',
					'prev_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Previous Post', 'gemcrypto' ) . '</span> ' .
						'<span class="screen-reader-text">' . __( 'Previous post:', 'gemcrypto' ) . '</span> <br/>' .
						'<span class="post-title">%title</span>',
				)
			);
		}

		// If comments are open or we have at least one comment, load up the comment template.
		if ( comments_open() || get_comments_number() ) {
			comments_template();
		}
	?>
</main>

	<?php
endwhile;
