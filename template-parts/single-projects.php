<?php
/**
 * The template for displaying singular post-types: projects, pages and user-defined custom post types.
 *
 * @package GemCryptoElementor
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>
<?php
while ( have_posts() ) :
	the_post();

	$client = get_field('project_client');
	$date = get_field('project_date');
	$tags = get_field('project_tags');
	$type = get_field('project_type');
	$socials = get_field('project_socials');
	?>

	<main class="site-main" role="main">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<?php if( has_post_thumbnail() ) { ?>
				<figure class="entry-thumbnail">
					<?php the_post_thumbnail( 'large' ); ?>
				</figure><!-- .entry-thumbnail -->
			<?php } ?>

			<div class="entry-content">
	      <?php
	        the_title( '<h1 class="entry-title">', '</h1>' );

	        the_terms( get_the_ID(), 'projects_category', '<div class="cat-links"><svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="bookmark" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512" class="svg-inline--fa fa-bookmark fa-w-12 fa-2x"><path fill="currentColor" d="M336 0H48C21.49 0 0 21.49 0 48v464l192-112 192 112V48c0-26.51-21.49-48-48-48zm0 428.43l-144-84-144 84V54a6 6 0 0 1 6-6h276c3.314 0 6 2.683 6 5.996V428.43z" class=""></path></svg> ', ', ', '</div>' );

					the_content();

					if( !empty( $client ) ) {
						echo '<div class="entry-meta entry-client"><span>' . esc_html__( 'Client: ', 'gemcrypto' ) . '</span>' . $client . '</div>';
					}

					if( !empty( $date ) ) {
						echo '<div class="entry-meta entry-date"><span>' . esc_html__( 'Date: ', 'gemcrypto' ) . '</span>' . $date . '</div>';
					}

					if( !empty( $tags ) ) {
						echo '<div class="entry-meta entry-tags"><span>' . esc_html__( 'Tags: ', 'gemcrypto' ) . '</span>' . $tags . '</div>';
					}

					if( !empty( $type ) ) {
						echo '<div class="entry-meta entry-type"><span>' . esc_html__( 'Project Type: ', 'gemcrypto' ) . '</span>' . $type . '</div>';
					}

					if( !empty( $socials ) ) {
						echo '<div class="entry-socials">';
							foreach ( $socials as $value ) {
								echo '<a class="_'.$value['social'].'" href="' . $value['link'] . '">';
								if( 'facebook' == $value['social'] ) {
									echo '<svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="facebook-f" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-facebook-f"><path fill="currentColor" d="M279.14 288l14.22-92.66h-88.91v-60.13c0-25.35 12.42-50.06 52.24-50.06h40.42V6.26S260.43 0 225.36 0c-73.22 0-121.08 44.38-121.08 124.72v70.62H22.89V288h81.39v224h100.17V288z" class=""></path></svg>';
								}

								if( 'twitter' == $value['social'] ) {
									echo '<svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="twitter" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-twitter"><path fill="currentColor" d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z" class=""></path></svg>';
								}

								if( 'instagram' == $value['social'] ) {
									echo '<svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="instagram" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-instagram"><path fill="currentColor" d="M224.1 141c-63.6 0-114.9 51.3-114.9 114.9s51.3 114.9 114.9 114.9S339 319.5 339 255.9 287.7 141 224.1 141zm0 189.6c-41.1 0-74.7-33.5-74.7-74.7s33.5-74.7 74.7-74.7 74.7 33.5 74.7 74.7-33.6 74.7-74.7 74.7zm146.4-194.3c0 14.9-12 26.8-26.8 26.8-14.9 0-26.8-12-26.8-26.8s12-26.8 26.8-26.8 26.8 12 26.8 26.8zm76.1 27.2c-1.7-35.9-9.9-67.7-36.2-93.9-26.2-26.2-58-34.4-93.9-36.2-37-2.1-147.9-2.1-184.9 0-35.8 1.7-67.6 9.9-93.9 36.1s-34.4 58-36.2 93.9c-2.1 37-2.1 147.9 0 184.9 1.7 35.9 9.9 67.7 36.2 93.9s58 34.4 93.9 36.2c37 2.1 147.9 2.1 184.9 0 35.9-1.7 67.7-9.9 93.9-36.2 26.2-26.2 34.4-58 36.2-93.9 2.1-37 2.1-147.8 0-184.8zM398.8 388c-7.8 19.6-22.9 34.7-42.6 42.6-29.5 11.7-99.5 9-132.1 9s-102.7 2.6-132.1-9c-19.6-7.8-34.7-22.9-42.6-42.6-11.7-29.5-9-99.5-9-132.1s-2.6-102.7 9-132.1c7.8-19.6 22.9-34.7 42.6-42.6 29.5-11.7 99.5-9 132.1-9s102.7-2.6 132.1 9c19.6 7.8 34.7 22.9 42.6 42.6 11.7 29.5 9 99.5 9 132.1s2.7 102.7-9 132.1z" class=""></path></svg>';
								}

								if( 'linkedin' == $value['social'] ) {
									echo '<svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="linkedin-in" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-linkedin-in"><path fill="currentColor" d="M100.28 448H7.4V148.9h92.88zM53.79 108.1C24.09 108.1 0 83.5 0 53.8a53.79 53.79 0 0 1 107.58 0c0 29.7-24.1 54.3-53.79 54.3zM447.9 448h-92.68V302.4c0-34.7-.7-79.2-48.29-79.2-48.29 0-55.69 37.7-55.69 76.7V448h-92.78V148.9h89.08v40.8h1.3c12.4-23.5 42.69-48.3 87.88-48.3 94 0 111.28 61.9 111.28 142.3V448z" class=""></path></svg>';
								}
								echo '</a>';
							}
						echo '</div>';
					}
	      ?>
			</div>
		</article><!-- #post-<?php the_ID(); ?> -->

		<div class="related-posts">
			<?php
				$args = array(
				'post_type' => 'projects',
				'post_status' => 'publish',
				'posts_per_page' => 3,
				'tax_query' => array(
					array(
						'taxonomy' => 'projects_category',
						'field' => 'id',
						'terms' => wp_get_object_terms( get_the_ID(), 'projects_category', array('fields' => 'ids') )
					)
				),
				'post__not_in' => array (get_the_ID()),
				);

				$related_items = new WP_Query( $args );

				// loop over query
				if ($related_items->have_posts()) :
				?>
					<h2 class="related-heading"><?php echo esc_html__( 'Projects Related', 'gemcrypto' ); ?></h3>
					<div class="projects-list">
						<?php while ( $related_items->have_posts() ) : $related_items->the_post(); ?>
							<div class="project-item">
								<?php if( has_post_thumbnail() ) { ?>
									<figure class="entry-thumbnail">
										<a class="entry-thumbnail-inner" href="<?php the_permalink(); ?>" aria-hidden="true" tabindex="-1">
											<?php the_post_thumbnail( 'medium_large' ); ?>
										</a>
									</figure><!-- .entry-thumbnail -->
								<?php } ?>

								<div class="entry-content">
									<?php
										 the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );

										 the_terms( get_the_ID(), 'projects_category', '<div class="cat-links">', ', ', '</div>' );
									?>
								</div><!-- .entry-content -->
							</div>
						<?php endwhile; ?>
					</div>
				<?php
				endif;
				wp_reset_postdata();
			?>
		</div>
	</main>

	<?php
endwhile;
