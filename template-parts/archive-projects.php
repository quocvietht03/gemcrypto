<?php
/**
 * The template for displaying archive projects pages.
 *
 * @package GemCryptoElementor
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>
<main class="site-main" role="main">

	<?php if ( apply_filters( 'gemcrypto_elementor_page_title', true ) ) : ?>
		<header class="page-header">
			<?php
			the_archive_title( '<h1 class="entry-title">', '</h1>' );
			the_archive_description( '<p class="archive-description">', '</p>' );
			?>
		</header>
	<?php endif; ?>
	<div class="page-content">
		<?php
		while ( have_posts() ) {
			the_post();
			?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<?php if( has_post_thumbnail() ) { ?>
					<figure class="entry-thumbnail">
						<a class="entry-thumbnail-inner" href="<?php the_permalink(); ?>" aria-hidden="true" tabindex="-1">
							<?php the_post_thumbnail( 'medium_large' ); ?>
						</a>
					</figure><!-- .entry-thumbnail -->
				<?php } ?>

				<div class="entry-content">
          <?php
				     the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );

             the_terms( get_the_ID(), 'projects_category', '<div class="cat-links">', ', ', '</div>' );
					?>
				</div><!-- .entry-content -->

			</article><!-- #post-<?php the_ID(); ?> -->
		<?php } ?>
	</div>

	<?php wp_link_pages(); ?>

	<?php
		// Posts pagination.
		the_posts_pagination(
			array(
				'mid_size'  => 2,
				'prev_text' => sprintf(
					'%s <span class="nav-prev-text">%s</span>',
					'<svg viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
					    <path d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z"></path>
					    <path d="M0 0h24v24H0z" fill="none"></path>
					</svg>',
					__( 'Prev', 'gemcrypto' )
				),
				'next_text' => sprintf(
					'<span class="nav-next-text">%s</span> %s',
					__( 'Next', 'gemcrypto' ),
					'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
					    <path d="M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z"></path>
					    <path d="M0 0h24v24H0z" fill="none"></path>
					</svg>'
				),
			)
		);
	?>
</main>
