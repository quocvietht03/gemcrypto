<?php
/**
 * Import pack data package demo
 *
 * @package Import Pack
 * @author BePlus
 */
$plugin_includes = array(
  array(
    'name'     => 'Elementor Website Builder',
    'slug'     => 'elementor',
  ),
  array(
    'name'     => 'Elementor Pro',
    'slug'     => 'elementor-pro',
    'source'   => IMPORT_REMOTE_SERVER_PLUGIN_DOWNLOAD . 'elementor-pro.zip',
  ),
  array(
    'name'     => 'Elementor Gemcrypto',
    'slug'     => 'elementor-gemcrypto',
    'source'   => IMPORT_REMOTE_SERVER_PLUGIN_DOWNLOAD . 'elementor-gemcrypto.zip',
  ),
  array(
    'name'     => 'Advanced Custom Fields PRO',
    'slug'     => 'advanced-custom-fields-pro',
    'source'   => IMPORT_REMOTE_SERVER_PLUGIN_DOWNLOAD . 'advanced-custom-fields-pro.zip',
  ),
  array(
    'name'     => 'Smash Balloon Instagram Feed',
    'slug'     => 'instagram-feed',
  ),

);

return apply_filters( 'beplus/import_pack/package_demo', [
    [
        'package_name' => 'gemcrypto-main',
        'preview' => IMPORT_URI . '/images/gemcrypto-main-preview.png', // image size 680x475
        'url_demo' => 'https://gemcrypto.beplusthemes.com/',
        'title' => __( 'Gemcrypto Main', 'gemcrypto' ),
        'description' => __( 'Gemcrypto main demo, include 39+ home demos & full inner page (Contact, About, Company, blog, etc.).' ),
        'plugins' => $plugin_includes,
    ],
] );
